FROM node:16.5

RUN apt-get update || : && apt-get install python3 python3-pip -y

WORKDIR /app

COPY ./requirements.txt ./requirements.txt
RUN python3 -m pip install -r requirements.txt

COPY . /app
CMD ["python3", "usage.py"]
