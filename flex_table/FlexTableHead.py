# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class FlexTableHead(Component):
    """A FlexTableHead component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.

Keyword arguments:

- columns (list; required)

- columnsWidth (dict; optional):
    The ID used to identify this component in Dash callbacks.

- filters (list; required)

- hiddenColumns (list; required)

- hoveredColumn (number; optional)

- sortDirection (string; optional)

- sortedColumnIndex (number; optional)"""
    @_explicitize_args
    def __init__(self, columnsWidth=Component.UNDEFINED, hoveredColumn=Component.UNDEFINED, onColumnHover=Component.UNDEFINED, onHideClick=Component.UNDEFINED, onSortClick=Component.UNDEFINED, onFilterClick=Component.UNDEFINED, onFilterChange=Component.UNDEFINED, sortedColumnIndex=Component.UNDEFINED, sortDirection=Component.UNDEFINED, hiddenColumns=Component.REQUIRED, filters=Component.REQUIRED, columns=Component.REQUIRED, **kwargs):
        self._prop_names = ['columns', 'columnsWidth', 'filters', 'hiddenColumns', 'hoveredColumn', 'sortDirection', 'sortedColumnIndex']
        self._type = 'FlexTableHead'
        self._namespace = 'flex_table'
        self._valid_wildcard_attributes =            []
        self.available_properties = ['columns', 'columnsWidth', 'filters', 'hiddenColumns', 'hoveredColumn', 'sortDirection', 'sortedColumnIndex']
        self.available_wildcard_properties =            []
        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs
        args = {k: _locals[k] for k in _explicit_args if k != 'children'}
        for k in ['hiddenColumns', 'filters', 'columns']:
            if k not in args:
                raise TypeError(
                    'Required argument `' + k + '` was not specified.')
        super(FlexTableHead, self).__init__(**args)
