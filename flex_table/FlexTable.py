# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class FlexTable(Component):
    """A FlexTable component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.

Keyword arguments:

- columns (list; required)

- rows (list; required):
    The ID used to identify this component in Dash callbacks."""
    @_explicitize_args
    def __init__(self, rows=Component.REQUIRED, columns=Component.REQUIRED, **kwargs):
        self._prop_names = ['columns', 'rows']
        self._type = 'FlexTable'
        self._namespace = 'flex_table'
        self._valid_wildcard_attributes =            []
        self.available_properties = ['columns', 'rows']
        self.available_wildcard_properties =            []
        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs
        args = {k: _locals[k] for k in _explicit_args if k != 'children'}
        for k in ['rows', 'columns']:
            if k not in args:
                raise TypeError(
                    'Required argument `' + k + '` was not specified.')
        super(FlexTable, self).__init__(**args)
