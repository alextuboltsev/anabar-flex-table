# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class FlexTableRow(Component):
    """A FlexTableRow component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.

Keyword arguments:

- columnsWidth (dict; optional)

- data (dict; required):
    The ID used to identify this component in Dash callbacks.

- hiddenColumns (list; required)

- style (dict; optional)"""
    @_explicitize_args
    def __init__(self, data=Component.REQUIRED, columnsWidth=Component.UNDEFINED, style=Component.UNDEFINED, onColumnHover=Component.UNDEFINED, hiddenColumns=Component.REQUIRED, **kwargs):
        self._prop_names = ['columnsWidth', 'data', 'hiddenColumns', 'style']
        self._type = 'FlexTableRow'
        self._namespace = 'flex_table'
        self._valid_wildcard_attributes =            []
        self.available_properties = ['columnsWidth', 'data', 'hiddenColumns', 'style']
        self.available_wildcard_properties =            []
        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs
        args = {k: _locals[k] for k in _explicit_args if k != 'children'}
        for k in ['data', 'hiddenColumns']:
            if k not in args:
                raise TypeError(
                    'Required argument `' + k + '` was not specified.')
        super(FlexTableRow, self).__init__(**args)
