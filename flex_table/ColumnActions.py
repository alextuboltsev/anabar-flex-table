# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class ColumnActions(Component):
    """A ColumnActions component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.

Keyword arguments:

- filterable (boolean; default True)

- isSorted (boolean; optional)

- isVisible (boolean; default False):
    The ID used to identify this component in Dash callbacks.

- sortDirection (string; default 'desc')

- sortable (boolean; default True)"""
    @_explicitize_args
    def __init__(self, isVisible=Component.UNDEFINED, isSorted=Component.UNDEFINED, sortDirection=Component.UNDEFINED, onHideClick=Component.REQUIRED, onSortClick=Component.REQUIRED, onFilterClick=Component.REQUIRED, filterable=Component.UNDEFINED, sortable=Component.UNDEFINED, **kwargs):
        self._prop_names = ['filterable', 'isSorted', 'isVisible', 'sortDirection', 'sortable']
        self._type = 'ColumnActions'
        self._namespace = 'flex_table'
        self._valid_wildcard_attributes =            []
        self.available_properties = ['filterable', 'isSorted', 'isVisible', 'sortDirection', 'sortable']
        self.available_wildcard_properties =            []
        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs
        args = {k: _locals[k] for k in _explicit_args if k != 'children'}
        for k in ['onHideClick', 'onSortClick', 'onFilterClick']:
            if k not in args:
                raise TypeError(
                    'Required argument `' + k + '` was not specified.')
        super(ColumnActions, self).__init__(**args)
