# AUTO GENERATED FILE - DO NOT EDIT

export ''_flextablehead

"""
    ''_flextablehead(;kwargs...)

A FlexTableHead component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `columns` (Array; required)
- `columnsWidth` (Dict; optional): The ID used to identify this component in Dash callbacks.
- `filters` (Array; required)
- `hiddenColumns` (Array; required)
- `hoveredColumn` (Real; optional)
- `sortDirection` (String; optional)
- `sortedColumnIndex` (Real; optional)
"""
function ''_flextablehead(; kwargs...)
        available_props = Symbol[:columns, :columnsWidth, :filters, :hiddenColumns, :hoveredColumn, :sortDirection, :sortedColumnIndex]
        wild_props = Symbol[]
        return Component("''_flextablehead", "FlexTableHead", "flex_table", available_props, wild_props; kwargs...)
end

