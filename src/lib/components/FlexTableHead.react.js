import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ColumnActions from './ColumnActions.react';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class FlexTableHead extends Component {
    constructor(props) {
        super(props);

        this.state = {
          filterValue: ''
        };

        this.filterInputs = {};
        
        this.handleTdMouseenter = this.handleTdMouseenter.bind(this);
        
        this.handleHideClick = this.handleHideClick.bind(this);
        this.handleSortClick = this.handleSortClick.bind(this);

        this.handleFilterClick = this.handleFilterClick.bind(this);
        this.handleFilterInput = this.handleFilterInput.bind(this);
        this.handleFilterBlur = this.handleFilterBlur.bind(this);
    }

    handleTdMouseenter(index) {
      this.props.onColumnHover(index);
    }

    handleHideClick(index) {
      this.props.onHideClick(index);
    }

    handleSortClick(index) {
      this.props.onSortClick(index);
    }

    handleFilterClick(index) {
      this.props.onFilterClick(index);
    }

    handleFilterInput(index, event) {
      this.setState({ filterValue: event.target.value });
      this.props.onFilterChange(index, event.target.value);
    }

    handleFilterBlur(index, event) {
      if (event.target.value === '') {
        this.props.onFilterChange(index, null);
      }
    }

    render() {

        return (
          <div className="th">
              <div className="tr">
                  <div className="td">
                      <div className="cell">27.05.2021&thinsp;—&thinsp;27.06.2021</div>
                  </div>
              </div>
              <div className="tr">
                  {this.props.columns.map((cell, index) => {
                      const isHidden = this.props.hiddenColumns.indexOf(index) > -1;
                      const hiddenStyle = isHidden 
                        ? { display: 'none' } 
                        : {};
                      const widthStyle = this.props.columnsWidth[index]
                        ? { 
                            width: `${this.props.columnsWidth[index]}em`,
                            flexShrink: 0
                          }
                        : {}
                      const cellStyle = Object.assign(hiddenStyle, widthStyle);

                      const classNames = [
                          'td',
                          cell.className,
                          (this.props.filters[index] !== null ? 'filtered' : '')
                      ];

                      const hasFilter = cell.filterable && this.props.filters[index] !== null;

                      return <div 
                        className={classNames.join(' ')} 
                        key={`th-${index}`}
                        onMouseEnter={(e) => this.handleTdMouseenter(index, e)}
                        style={cellStyle}
                      >
                          <div className="cell">
                            {cell.type === 'number'
                              ? <span className="number-string" data-index={index} dangerouslySetInnerHTML={{ __html: cell.content }}></span>
                              : <span dangerouslySetInnerHTML={{ __html: cell.content }}></span>
                            }                            
                            
                            <ColumnActions 
                              isVisible={this.props.hoveredColumn === index} 
                              isSorted={this.props.sortedColumnIndex === index} 
                              sortDirection={cell.sortDirection}
                              onHideClick={(e) => this.handleHideClick(index, e)}
                              onSortClick={(e) => this.handleSortClick(index, e)}
                              onFilterClick={(e) => this.handleFilterClick(index, e)}
                              sortable={cell.sortable}
                              filterable={cell.filterable}
                            />
                            <div 
                              className="columns-filter"
                              style={(!hasFilter ? {display: 'none'} : {})}
                            >
                              <input 
                                type="text" 
                                onChange={(e) => this.handleFilterInput(index, e)} 
                                onBlur={(e) => this.handleFilterBlur(index, e)}
                                value={this.state.filterValue}
                                ref={(instance) => {this.filterInputs[index] = instance;}}
                                placeholder="Найти"
                              />
                            </div>
                          </div>
                      </div>;
                  })}
              </div>
          </div>
        );
    }
}

FlexTableHead.defaultProps = {
};

FlexTableHead.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    columnsWidth: PropTypes.object,

    hoveredColumn: PropTypes.number,

    onColumnHover: PropTypes.func,

    onHideClick: PropTypes.func,

    onSortClick: PropTypes.func,

    onFilterClick: PropTypes.func,

    onFilterChange: PropTypes.func,

    sortedColumnIndex: PropTypes.number,

    sortDirection: PropTypes.string,

    hiddenColumns: PropTypes.array.isRequired,

    filters: PropTypes.array.isRequired,

    columns: PropTypes.array.isRequired,

    // key: PropTypes.string.isRequired,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
