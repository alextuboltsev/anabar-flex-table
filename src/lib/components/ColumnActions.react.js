import React, {Component} from 'react';
import PropTypes from 'prop-types';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class ColumnActions extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        const classNames = [
            'actions',
            (!this.props.isVisible ? 'hidden' : ''),
            (this.props.isSorted ? 'sorted' : '')
        ];

        return (
          <div className={classNames.join(' ')}>
              {this.props.sortable &&
                  <button className={`actions__btn actions__btn--sort actions__btn--sort-${this.props.sortDirection}`} onClick={this.props.onSortClick}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="14px" height="14px" viewBox="-139.5 180.5 280 280">
                        <path d="M122.5,342.3L78.9,386l-43.6-43.7H68V255h21.8v87.3H122.5z M-117.5,255H35.2v21.8h-152.7V255z M-117.5,309.5H13.4v21.8 h-130.9V309.5z M-117.5,364.1h98.2v21.8h-98.2V364.1z"/>
                      </svg>
                  </button>
              }

              {this.props.filterable &&
                <button className="actions__btn actions__btn--filter" onClick={this.props.onFilterClick}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="14px" height="14px" viewBox="-139.5 180.5 280 280">
                        <path d="M-12.6,256.1c28.3,0,51.6,23,51.6,51.6c0,10.8-3.4,20.9-9.6,29.8l-5,7.2l-7.2,5c-8.9,6.2-19,9.6-29.8,9.6 c-28.3,0-51.6-23-51.6-51.6C-64.2,279.1-40.9,256.1-12.6,256.1 M-12.6,230.4c-42.7,0-77.3,34.6-77.3,77.3s34.6,77.3,77.3,77.3 c16.6,0,31.9-5.3,44.6-14.4l36.2,36.2c2.4,2.4,5.8,3.8,9.1,3.8c3.4,0,6.7-1.2,9.1-3.8l0.2-0.2c5-5,5.8-13.4,0-18.2l-36.2-36 c8.9-12.7,14.4-28.1,14.4-44.6C64.7,264.9,30.2,230.4-12.6,230.4L-12.6,230.4z"/>
                    </svg>
                </button>
              }

              <button className="actions__btn actions__btn--hide" onClick={this.props.onHideClick}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="14px" height="14px" viewBox="-20.1 -10 280 280">
                      <path d="M220.4,120.2c-7.4-8-18.3-17.9-32.6-26.6l-33.3,33.3c0.7,2.5,0.9,5.1,0.9,7.6c0,19.6-15.8,35.7-35.5,35.7c-2.7,0-5.1-0.2-7.8-0.9l-21.9,21.9c9.1,2.2,19,3.6,29.7,3.6c49.8,0,83.7-27.9,100.4-45.7C227.8,140.7,227.8,128.4,220.4,120.2z"/>
                      <path d="M200.8,44.8c-4.5-4.5-11.6-4.5-16.1,0l-33.5,33.5c-9.6-2.5-20.1-3.8-31.2-3.8c-49.8,0-83.7,27.9-100.4,45.7c-7.6,8-7.6,20.5,0,28.8c7.6,8.3,19,18.5,33.7,27.2l-23,23c-4.5,4.5-4.5,11.6,0,16.1c2.2,2.2,5.1,3.3,8,3.3c2.9,0,5.8-1.1,8-3.3L200.8,60.8C205.2,56.4,205.2,49.2,200.8,44.8z M84.5,134.5c0-19.6,15.8-35.5,35.5-35.5c3.1,0,6.2,0.4,9.4,1.3l-43.7,43.5
                        C85,140.9,84.5,137.8,84.5,134.5z"/>
                  </svg>
              </button>
          </div>
        );
    }
}

ColumnActions.defaultProps = {
  isVisible: false,
  sortDirection: 'desc',
  filterable: true,
  sortable: true
};

ColumnActions.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    isVisible: PropTypes.bool,

    isSorted: PropTypes.bool,

    sortDirection: PropTypes.string,
    
    onHideClick: PropTypes.func.isRequired,

    onSortClick: PropTypes.func.isRequired,

    onFilterClick: PropTypes.func.isRequired,
    
    filterable: PropTypes.bool,

    sortable: PropTypes.bool,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
