import React, {Component} from 'react';
import PropTypes from 'prop-types';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class HiddenColumnsBar extends Component {
    constructor(props) {
        super(props);
        this.handleHiddenClick = this.handleHiddenClick.bind(this);
        this.handleShowAllClick = this.handleShowAllClick.bind(this);
    }

    handleHiddenClick(index) {
        this.props.onHiddenClick(index);
    }

    handleShowAllClick() {
        this.props.onShowAllClick();
    }

    render() {
        const hiddenColumns = this.props.columns
            .map((item, index) => ({...item, index}))
            .filter((item) => this.props.hiddenColumns.indexOf(item.index) > -1);

        return (
            <div className="hidden-columns">
                <span>Вернуть скрытые колонки: </span>
                {hiddenColumns.map((column, index) => {
                    return <span key={`hidden-col-${index}`}>
                        <span 
                            className="hidden-columns__item"
                            dangerouslySetInnerHTML={{ __html: column.content }}
                            onClick={() => this.handleHiddenClick(column.index)}
                        >
                        </span>
                        {index < hiddenColumns.length - 1 && <>, </>}
                    </span>;
                })}
                <span className="hidden-columns__item hidden-columns__item--all" onClick={this.handleShowAllClick}>Вернуть все</span>
            </div>
        );
    }
}

HiddenColumnsBar.defaultProps = {
};

HiddenColumnsBar.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    columns: PropTypes.array.isRequired,

    hiddenColumns: PropTypes.array.isRequired,

    onHiddenClick: PropTypes.func.isRequired,
    onShowAllClick: PropTypes.func.isRequired,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
