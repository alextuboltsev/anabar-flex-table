import React, {Component} from 'react';
import PropTypes from 'prop-types';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class FlexTableSettings extends Component {
    constructor(props) {
        super(props);
        this.handleHeightChange = this.handleHeightChange.bind(this);
        this.handleScaleChange = this.handleScaleChange.bind(this);
    }

    handleHeightChange(event) {
      this.props.onHeightChange(event);
    }

    handleScaleChange(event) {
      this.props.onScaleChange(event);
    }
    
    render() {        
        return (
          <div className="flex-table-settings">
              <div className="flex-table-setting">
                  <label htmlFor="">Высота</label>
                  <input 
                      type="range"
                      value={this.props.rowHeight}
                      onChange={this.handleHeightChange}
                      min="1.5" 
                      max="10" 
                      step="0.1" 
                  />
              </div>

              <div className="flex-table-setting">
                  <label htmlFor="">Масштаб</label>

                  <input
                      type="range" 
                      value={this.props.tableScale}
                      onChange={this.handleScaleChange}
                      min="12" 
                      max="28" 
                      step="0.5" 
                  />
              </div>
          </div>
        );
    }
}

FlexTableSettings.defaultProps = {
};

FlexTableSettings.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    rowHeight: PropTypes.number.isRequired,
    tableScale: PropTypes.number.isRequired,

    onHeightChange: PropTypes.func.isRequired,
    onScaleChange: PropTypes.func.isRequired,

    // key: PropTypes.string.isRequired,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
