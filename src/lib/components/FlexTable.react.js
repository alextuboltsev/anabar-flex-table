import React, {Component} from 'react';
import PropTypes from 'prop-types';
import FlexTableSettings from './FlexTableSettings.react';
import FlexTableHead from './FlexTableHead.react';
import HiddenColumnsBar from './HiddenColumnsBar.react';
import FlexTableRow from './FlexTableRow.react';

const sortTable = (items, key, direction) => {
    return items.sort((a, b) => {
        if (a[key] < b[key]) {
            return direction === 'asc' ? -1 : 1;
        }
        if (a[key] > b[key]) {
            return direction === 'asc' ? 1 : -1;
        }
        return 0;

    });
};

const toggleValue = (value, a, b) => {
    return value === a ? b : a;
};

// const useSortableData = (items, config = null) => {
//     const [sortConfig, setSortConfig] = React.useState(config);
  
//     const sortedItems = React.useMemo(() => {
//         let sortableItems = [...items];
//         if (sortConfig !== null) {
//             sortableItems.sort((a, b) => {
//                 if (a[sortConfig.key] < b[sortConfig.key]) {
//                     return sortConfig.direction === 'ascending' ? -1 : 1;
//                 }
//                 if (a[sortConfig.key] > b[sortConfig.key]) {
//                     return sortConfig.direction === 'ascending' ? 1 : -1;
//                 }
//                 return 0;
//             });
//         }
//         return sortableItems;
//     }, [items, sortConfig]);
  
//     const requestSort = (key) => {
//         let direction = 'ascending';
//         if (
//             sortConfig &&
//             sortConfig.key === key &&
//             sortConfig.direction === 'ascending'
//         ) {
//             direction = 'descending';
//         }
//         setSortConfig({ key, direction });
//     };
  
//     return { items: sortedItems, requestSort, sortConfig };
// };

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class FlexTable extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            rowHeight: 4.5,
            tableScale: 20,
            sortedColumnIndex: null,
            sortDirection: null,
            // sortedRows: [...this.props.rows],
            sortedRows: this.props.rows,
            columns: this.props.columns,
            hoveredColumn: null,
            hiddenColumns: [],
            columnsWidth: {},
            filters: this.props.columns.map(() => null),
            filteredRows: this.props.rows
        };

        this.table = React.createRef();
        this.cellGradientPaddingInEm = 1;

        this.handleHeightChange = this.handleHeightChange.bind(this);
        this.handleScaleChange = this.handleScaleChange.bind(this);
        this.handleColumnHover = this.handleColumnHover.bind(this);
        this.handleTableMouseleave = this.handleTableMouseleave.bind(this);

        this.handleSortClick = this.handleSortClick.bind(this);

        this.handleHideClick = this.handleHideClick.bind(this);
        this.handleShowClick = this.handleShowClick.bind(this);
        this.handleShowAllClick = this.handleShowAllClick.bind(this);

        this.handleFilterClick = this.handleFilterClick.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }

    handleHeightChange(event) {
        this.setState({rowHeight: event.target.value});
    }

    handleScaleChange(event) {
        this.setState({tableScale: event.target.value});
    }

    handleColumnHover(index) {
        this.setState({hoveredColumn: index});
    }

    handleTableMouseleave(event) {
        this.setState({hoveredColumn: null});
    }

    handleSortClick(index) {
        let direction;
        let currentColumn;

        if (this.state.sortedColumnIndex !== index) {
            this.setState({sortedColumnIndex: index});
            
            currentColumn = this.state.columns[index];
            direction = 'asc';
        } else {
            currentColumn = this.state.columns[this.state.sortedColumnIndex];
            direction = toggleValue(currentColumn.sortDirection, 'asc', 'desc');
        }

        this.setState((state) => {
            const updatedColumns = [...state.columns].map((item, itemIndex) => {
                item.sortDirection = itemIndex === index ? direction : 'desc';
                return item;
            });

            return { columns: updatedColumns };
        });

        const sortedRows = sortTable(
            this.props.rows, 
            currentColumn.sortKey, 
            direction
        );

        this.setState({sortedRows: sortedRows});
    }

    handleHideClick(index) {
        this.setState((state) => {
            return {
                hiddenColumns: [...state.hiddenColumns, index]
            };
        });
    }

    handleShowClick(index) {
        this.setState((state) => {
            return {
                hiddenColumns: [...state.hiddenColumns.filter((item) => item !== index)]
            };
        });
    }

    handleShowAllClick() {
        this.setState({hiddenColumns: []});
    }

    handleFilterClick(index) {
        this.setState((state) => {
            const updatedFilters = state.filters.map((item, itemIndex) => {
                if (itemIndex === index) {
                    return item !== null ? null : {
                        key: state.columns[index].filterKey,
                        value: ''
                    };
                }

                return item;
            });

            return {
                filters: updatedFilters
            }
        });
    }

    handleFilterChange(index, value) {

        const updatedFilters = [...this.state.filters];
        updatedFilters[index] = value !== null 
            ? {
                key: this.state.columns[index].filterKey,
                value: value
            }
            : null;

        let filteredRows = [];

        const activeFilters = updatedFilters.filter(item => item !== null);

        if (!activeFilters.length) {
            filteredRows = [...this.state.sortedRows]
        } else {
            filteredRows = [...this.state.sortedRows].filter((row) => {
                for (const filter of activeFilters) {
                    const cellContent = String(row[filter.key]);
                    if (!cellContent.toLowerCase().includes(filter.value.toLowerCase())) {
                        return false;
                    }
                }

                return true;
            });
        }

        this.setState({
            filters: updatedFilters,
            filteredRows: filteredRows
        });
    }

    componentDidMount() {
        // const allNumberCellWidth = [...this.table.current.querySelectorAll('.number-string')].reduce((list, item) => {
        //     console.log(item.dataset.index)
        //     console.log(list[item.dataset.index])
        //     if (list[item.dataset.index]) {
        //         list[item.dataset.index].push(item.offsetWidth);
        //     } else {
        //         list[item.dataset.index] = [item.offsetWidth]
        //     }
            
        //     return list;
        // }, {});

        // const columnsWidthByIndex = {} 
        // Object.entries(allNumberCellWidth).forEach((entry) => {
        //     const [key, value] = entry;

        //     columnsWidthByIndex[key] = (Math.max(...value) / this.state.tableScale) + this.cellGradientPaddingInEm;
        // });

        // this.setState({
        //     columnsWidth: columnsWidthByIndex
        // });
    }

    render() {
        // const { rows } = this.props;

        const TableStyle = {
            fontSize: `${this.state.tableScale}px`
        }

        const rowStyle = {
            height: `${this.state.rowHeight}em`
        };        
        
        return (
            <div className="ids__wrapper ids__full-width">
                <FlexTableSettings
                    rowHeight={parseInt(this.state.rowHeight, 10)}
                    tableScale={parseInt(this.state.tableScale, 10)}
                    onHeightChange={this.handleHeightChange}
                    onScaleChange={this.handleScaleChange}
                />

                {this.state.hiddenColumns.length > 0 &&
                    <HiddenColumnsBar
                        columns={this.state.columns}
                        hiddenColumns={this.state.hiddenColumns}
                        onHiddenClick={this.handleShowClick}
                        onShowAllClick={this.handleShowAllClick}
                    />
                }

                <div 
                    className="anabar__flex-table" 
                    style={TableStyle}
                    onMouseLeave={this.handleTableMouseleave}
                    ref={this.table}
                >
                    <FlexTableHead 
                        columns={this.state.columns}
                        columnsWidth={this.state.columnsWidth}
                        hoveredColumn={this.state.hoveredColumn}
                        sortedColumnIndex={this.state.sortedColumnIndex}
                        sortDirection={this.state.sortDirection}
                        hiddenColumns={this.state.hiddenColumns}
                        filters={this.state.filters}
                        onColumnHover={this.handleColumnHover}
                        onSortClick={this.handleSortClick}
                        onHideClick={this.handleHideClick}
                        onFilterClick={this.handleFilterClick}
                        onFilterChange={this.handleFilterChange}
                    />
                    <div className="tbody">
                        {this.state.filteredRows.map((row, index) => (
                            <FlexTableRow
                                data={row}
                                key={`row-${index}`}
                                style={rowStyle}
                                columnsWidth={this.state.columnsWidth}
                                hiddenColumns={this.state.hiddenColumns}
                                onColumnHover={this.handleColumnHover}
                            />
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

FlexTable.defaultProps = {};

FlexTable.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    rows: PropTypes.array.isRequired,

    columns: PropTypes.array.isRequired,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
