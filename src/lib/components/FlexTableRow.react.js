import React, {Component} from 'react';
import PropTypes, { number } from 'prop-types';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class FlexTableRow extends Component {
    constructor(props) {
        super(props);

        // this.row = React.createRef();

        // this.state = {
        //     columnsWidth: {}
        // };

        this.handleTdMouseenter = this.handleTdMouseenter.bind(this);
    }

    handleTdMouseenter(index) {
        this.props.onColumnHover(index);
    }

    // handleTdMouseleave(index) {
    // }
    
    render() {
        const data = this.props.data
        const row = [
          {
            className: 'vendor-code',
            content: <a href={data.link}>{data.vendorCode}</a>
          },
          {
            className: 'photo hover-show',
            content: <img src={data.photo} />
          },
          {
            className: 'sales-graphic',
            content: <img src={data.graphic} />
          },
          {
            className: 'brand',
            content: <a href={data.brandLink}>{data.brand}</a>
          },
          {
            className: 'vendor hover-show',
            content: data.vendor
          },
          {
            className: 'category hover-show',
            content: <div dangerouslySetInnerHTML={{ __html: data.category }}></div>
          },
          {
            className: 'description hover-show',
            content: data.descr
          },
          {
            className: 'align-right min-price',
            type: 'number',
            content: `${data.minPrice} ₽`
          },
          {
            className: 'align-right max-price',
            type: 'number',
            content: `${data.maxPrice} ₽`
          },
          {
            className: 'align-right orders',
            type: 'number',
            content: data.orders
          },
          {
            className: 'align-right proceeds',
            type: 'number',
            content: `${data.proceeds} ₽`
          },
        ]

        return (
          <div 
            className="tr" 
            style={this.props.style}
          >
              {row.map((cell, index) => {
                  const isHidden = this.props.hiddenColumns.indexOf(index) > -1;
                  const hiddenStyle = isHidden 
                    ? { display: 'none' } 
                    : {};
                  const widthStyle = this.props.columnsWidth[index]
                    ? { 
                        width: `${this.props.columnsWidth[index]}em`,
                        flexShrink: 0
                      }
                    : {}
                  const cellStyle = Object.assign(hiddenStyle, widthStyle);

                  return <div 
                    className={`td ${cell.className}`} 
                    key={`cell-${index}`}
                    onMouseEnter={(e) => this.handleTdMouseenter(index, e)}
                    style={cellStyle}
                  >
                      <div className="cell">
                          {cell.type === 'number'
                            ? <span className="number-string" data-index={index}>{cell.content}</span>
                            : cell.content
                          }
                      </div>
                  </div>
              })}
          </div>
        );
    }
}

FlexTableRow.defaultProps = {
  style: {}
};

FlexTableRow.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    data: PropTypes.object.isRequired,

    columnsWidth: PropTypes.object,

    style: PropTypes.object,

    onColumnHover: PropTypes.func,

    hiddenColumns: PropTypes.array.isRequired,

    // key: PropTypes.string.isRequired,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
