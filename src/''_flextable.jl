# AUTO GENERATED FILE - DO NOT EDIT

export ''_flextable

"""
    ''_flextable(;kwargs...)

A FlexTable component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `columns` (Array; required)
- `rows` (Array; required): The ID used to identify this component in Dash callbacks.
"""
function ''_flextable(; kwargs...)
        available_props = Symbol[:columns, :rows]
        wild_props = Symbol[]
        return Component("''_flextable", "FlexTable", "flex_table", available_props, wild_props; kwargs...)
end

