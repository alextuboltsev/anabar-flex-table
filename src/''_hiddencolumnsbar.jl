# AUTO GENERATED FILE - DO NOT EDIT

export ''_hiddencolumnsbar

"""
    ''_hiddencolumnsbar(;kwargs...)

A HiddenColumnsBar component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `columns` (Array; required): The ID used to identify this component in Dash callbacks.
- `hiddenColumns` (Array; required)
"""
function ''_hiddencolumnsbar(; kwargs...)
        available_props = Symbol[:columns, :hiddenColumns]
        wild_props = Symbol[]
        return Component("''_hiddencolumnsbar", "HiddenColumnsBar", "flex_table", available_props, wild_props; kwargs...)
end

