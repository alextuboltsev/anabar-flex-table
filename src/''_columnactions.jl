# AUTO GENERATED FILE - DO NOT EDIT

export ''_columnactions

"""
    ''_columnactions(;kwargs...)

A ColumnActions component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `filterable` (Bool; optional)
- `isSorted` (Bool; optional)
- `isVisible` (Bool; optional): The ID used to identify this component in Dash callbacks.
- `sortDirection` (String; optional)
- `sortable` (Bool; optional)
"""
function ''_columnactions(; kwargs...)
        available_props = Symbol[:filterable, :isSorted, :isVisible, :sortDirection, :sortable]
        wild_props = Symbol[]
        return Component("''_columnactions", "ColumnActions", "flex_table", available_props, wild_props; kwargs...)
end

