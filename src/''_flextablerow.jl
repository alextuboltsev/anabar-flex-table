# AUTO GENERATED FILE - DO NOT EDIT

export ''_flextablerow

"""
    ''_flextablerow(;kwargs...)

A FlexTableRow component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `columnsWidth` (Dict; optional)
- `data` (Dict; required): The ID used to identify this component in Dash callbacks.
- `hiddenColumns` (Array; required)
- `style` (Dict; optional)
"""
function ''_flextablerow(; kwargs...)
        available_props = Symbol[:columnsWidth, :data, :hiddenColumns, :style]
        wild_props = Symbol[]
        return Component("''_flextablerow", "FlexTableRow", "flex_table", available_props, wild_props; kwargs...)
end

