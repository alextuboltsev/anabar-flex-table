
module FlexTable
using Dash

const resources_path = realpath(joinpath( @__DIR__, "..", "deps"))
const version = "0.0.1"

include("''_columnactions.jl")
include("''_flextable.jl")
include("''_flextablehead.jl")
include("''_flextablerow.jl")
include("''_flextablesettings.jl")
include("''_hiddencolumnsbar.jl")

function __init__()
    DashBase.register_package(
        DashBase.ResourcePkg(
            "flex_table",
            resources_path,
            version = version,
            [
                DashBase.Resource(
    relative_package_path = "flex_table.min.js",
    external_url = nothing,
    dynamic = nothing,
    async = nothing,
    type = :js
),
DashBase.Resource(
    relative_package_path = "flex_table.min.js.map",
    external_url = nothing,
    dynamic = true,
    async = nothing,
    type = :js
)
            ]
        )

    )
end
end
