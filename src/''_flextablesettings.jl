# AUTO GENERATED FILE - DO NOT EDIT

export ''_flextablesettings

"""
    ''_flextablesettings(;kwargs...)

A FlexTableSettings component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `rowHeight` (Real; required): The ID used to identify this component in Dash callbacks.
- `tableScale` (Real; required)
"""
function ''_flextablesettings(; kwargs...)
        available_props = Symbol[:rowHeight, :tableScale]
        wild_props = Symbol[]
        return Component("''_flextablesettings", "FlexTableSettings", "flex_table", available_props, wild_props; kwargs...)
end

