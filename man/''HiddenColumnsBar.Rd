% Auto-generated: do not edit by hand
\name{''HiddenColumnsBar}

\alias{''HiddenColumnsBar}

\title{HiddenColumnsBar component}

\description{
ExampleComponent is an example component. It takes a property, `label`, and displays it. It renders an input with the property `value` which is editable by the user.
}

\usage{
''HiddenColumnsBar(columns=NULL, hiddenColumns=NULL, onHiddenClick=NULL,
onShowAllClick=NULL)
}

\arguments{
\item{columns}{Unnamed list. The ID used to identify this component in Dash callbacks.}

\item{hiddenColumns}{Unnamed list. }

\item{onHiddenClick}{}

\item{onShowAllClick}{}
}

\value{named list of JSON elements corresponding to React.js properties and their values}

